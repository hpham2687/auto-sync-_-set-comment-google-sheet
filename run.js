var urlOfSheetData = "https://docs.google.com/spreadsheets/d/1osZLMyF_exEKm_1blYOm7fPV8n8QlrxqUejhRK__99U/edit";
var targetSheetName = "Trang tính3";
var sourceSheetName = "Trang tính2";

// set up link above

function goToEnd(x, y, cmt) {
    //var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();  

  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(targetSheetName);


    var lastRow = sheet.getLastRow();
    var lastColumn = sheet.getLastColumn();

    var lastCell = sheet.getRange(x, y, 1, 1);
    //Browser.msgBox('values: ' + lastRow + '-' + lastColumn);
    sheet.setCurrentCell(lastCell);


    var comments = lastCell.getComment();
    // Newline works in msgBox, but not in Note.
    comments = cmt;
    //Browser.msgBox(comments);
    lastCell.setComment(comments);


}

function addComment() {
  
// The code below logs the index of a sheet named "Expenses"
var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sourceSheetName);
  
    var dulieu = [];
 //   var ss = SpreadsheetApp.getActiveSpreadsheet().getSheets();
   // var sheet = thisSheet;

    var dt = sheet.getDataRange().getValues();
  
      var startCell = sheet.getRange(1, 8, 1, 1).getValue();
      
  var endCell;
    for (var i = startCell; i < dt.length; i++) {
      if (dt[i][0] == ''){ endCell = i; 
                          sheet.getRange(1, 8, 1, 1).setValue(endCell);                          
                          break;                         
                         
                         }
        var oneData = {
            time: "" + dt[i][0] + "",
            name: "" + dt[i][2] + "",
            cell: [dt[i][4], dt[i][5]]
        };
        dulieu.push(oneData)
    }
    //   Logger.log(dulieu)
    var i;
    for (i = 0; i < dulieu.length; i++) {
        var cmt = 'this cell ís taken by ' + dulieu[i].name + ' at ' + dulieu[i].time;
        goToEnd(dulieu[i].cell[0], dulieu[i].cell[1], cmt);
    }
}

function synchRange() {

    var ss = SpreadsheetApp.getActiveSpreadsheet().getSheets();
    Logger.log(ss[1].getName());
    var sheet = ss[1];
    var timeSpanCell = sheet.getRange(1, 1, 1, 1);
    sheet.setCurrentCell(timeSpanCell);
    timeSpanCell.setValue('=IMPORTRANGE("' + urlOfSheetData + '"; "A1:A1000")');

    var nameCell = sheet.getRange(1, 3, 1, 1);
    sheet.setCurrentCell(nameCell);
    nameCell.setValue('=IMPORTRANGE("' + urlOfSheetData + '"; "C1:C1000")');

    var hangCell = sheet.getRange(1, 5, 1, 1);
    sheet.setCurrentCell(hangCell);
    hangCell.setValue('=IMPORTRANGE("' + urlOfSheetData + '"; "E1:E1000")');

    var cotCell = sheet.getRange(1, 6, 1, 1);
    sheet.setCurrentCell(cotCell);
    cotCell.setValue('=IMPORTRANGE("' + urlOfSheetData + '"; "F1:F1000")');

}